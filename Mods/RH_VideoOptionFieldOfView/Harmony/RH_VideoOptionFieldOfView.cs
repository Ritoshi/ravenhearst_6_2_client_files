﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;

public class RH_VideoOptionFieldOfView
{
    public class RH_VideoOptionFieldOfView_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("Init")]
    class PatchXUiC_OptionsVideoInit
    {
        static void Postfix(XUiC_OptionsVideo __instance)
        {
            __instance.comboFieldOfViewNew = __instance.GetChildById("FieldOfViewNew").GetChildByType<XUiC_ComboBoxInt>();
            __instance.comboFieldOfViewNew.OnValueChangedGeneric += __instance.AnyPresetValueChanged;
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("applyChanges")]
    public class PatchXUiC_OptionsVideoapplyChanges
    {
        static bool Prefix(XUiC_OptionsVideo __instance)
        {
            GamePrefs.Set(EnumGamePrefs.OptionsFieldOfViewNew, (int)__instance.comboFieldOfViewNew.Value);

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("updateGraphicOptions")]
    public class PatchXUiC_OptionsVideoupdateGraphicOptions
    {
        static bool Prefix(XUiC_OptionsVideo __instance)
        {
            __instance.comboFieldOfViewNew.Value = GamePrefs.GetInt(EnumGamePrefs.OptionsFieldOfViewNew);

            return true;
        }
    }
}
