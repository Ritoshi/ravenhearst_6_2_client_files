﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class RH_GameOptionRage
{
    public class RH_GameOptionRage_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityZombie))]
    [HarmonyPatch("ProcessDamageResponseLocal")]
    [HarmonyPatch(new Type[] { typeof(DamageResponse) })]
    public class PatchEntityZombieProcessDamageResponseLocal
    {
        static void Postfix(EntityZombie __instance, ref DamageResponse _dmResponse)
        {
            if (__instance.PlayerGamestage == 0) // had to move this from init to prefix ProcessDamageResponseLocal as on INIT the zombie spawns at position 0,0,0 and hence would never find players within a 1000m area to calulate the GS
            {
                __instance.PlayerGamestage = GetGamestageFromBounds(__instance.position.x, __instance.position.y, __instance.position.z, "");
            }

            if (!IsRageSettingEnabled(GameStats.GetInt(EnumGameStats.ZombieRage), __instance.PlayerGamestage))
            {
                return;
            }

            if (!__instance.isEntityRemote && !__instance.IsRaged)
            {
                int @int = GameStats.GetInt(EnumGameStats.GameDifficulty);
                float num = EntityZombie.rageChances[@int];
                if (_dmResponse.Source.GetDamageType() == EnumDamageTypes.BloodLoss)
                {
                    num *= 0.1f;
                }
                if (__instance.rand.RandomFloat < num)
                {
                    __instance.moveSpeedRagePer = 0.5f + __instance.rand.RandomFloat * 0.5f;
                    __instance.moveSpeedScaleTime = 4f + __instance.rand.RandomFloat * 6f;

                    if (IsRageSettingEnabled(GameStats.GetInt(EnumGameStats.ZombieSuperRage), __instance.PlayerGamestage) && __instance.rand.RandomFloat < EntityZombie.superRageChances[@int])
                    {
                        __instance.moveSpeedRagePer = 2f;
                        __instance.moveSpeedScaleTime = 30f;
                        __instance.PlayOneShot(__instance.GetSoundAlert(), false);
                    }

                    // If OnceRagedAlwaysRage is set in GameStats then zomebie will now be permanetely raged
                    if (IsRageSettingEnabled(GameStats.GetInt(EnumGameStats.OnceRagedAlwaysRage), __instance.PlayerGamestage))
                    {
                        __instance.IsRaged = true;
                        __instance.moveSpeedScaleTime = 100000;
                    }
                }
            }
        }
    }

    private static bool IsRageSettingEnabled(int gameOptionValue, int playerGamestage)
    {
        //0 = on
        //1 = off
        //2 = 10
        //3 = 20
        //4 = 30
        //5 = 50
        //6 = 100

        switch (gameOptionValue)
        {
            case 0:
                return true;
            case 1:
                return false;
            case 2:
                return playerGamestage >= 10;
            case 3:
                return playerGamestage >= 20;
            case 4:
                return playerGamestage >= 30;
            case 5:
                return playerGamestage >= 50;
            case 6:
                return playerGamestage >= 100;
        }

        return false;
    }

    public static int GetGamestageFromBounds(float x, float y, float z, string source, bool log = false)
    {
        if (log)
            Log.Out("BiomeSpawning : " + x + " : " + y + " : " + z );

        // Get Players gamestage
        var chunkPosition = new Vector3(x, y, z);
        var bounds = new Bounds(new Vector3(chunkPosition.x, chunkPosition.y, chunkPosition.z), new Vector3(200f, 200f, 200f));
        List<Entity> playersInBounds = GameManager.Instance.World.GetEntitiesInBounds(typeof(EntityPlayer), bounds, new List<Entity>());
        List<int> partyLevelList = new List<int>();
        foreach (var entityBound in playersInBounds)
        {
            var player = entityBound as EntityPlayer;
            partyLevelList.Add(player.gameStage);

            if (log)
                Log.Out("BiomeSpawning Player in bound : " + player.entityId + " : GS = " + player.gameStage + " : source = " + source);
        }

        return Mathf.Max(GameStageDefinition.CalcPartyLevel(partyLevelList), 1);
    }

}
