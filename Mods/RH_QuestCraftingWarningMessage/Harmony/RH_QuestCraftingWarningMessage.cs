﻿using Harmony;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_QuestCraftingWarningMessage
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_WorkstationGrid))]
    [HarmonyPatch("OnClose")]
    public class PatchXUiC_WorkstationGridOnClose
    {
        // NOTE : Needs new entry in Localization
        // questItemCrafting,,Quest Info,New,[FF0000]WARNING[-] - You are currently crafting a quest item in that workstation you just closed!,,,,

        static void Postfix(XUiC_WorkstationGrid __instance)
        {
            if (__instance.workstationData != null)
            {
                if (__instance.xui.playerUI.entityPlayer.QuestJournal.quests == null)
                    return;

                List<int> list = new List<int>();
                foreach (var quest in __instance.xui.playerUI.entityPlayer.QuestJournal.quests)
                {
                    if ((quest.CurrentState == Quest.QuestState.InProgress || quest.CurrentState == Quest.QuestState.NotStarted) && quest.Objectives.Count > 0)
                    {
                        foreach (BaseObjective baseObjective in quest.Objectives.ToArray())
                        {
                            if (baseObjective.ObjectiveState != BaseObjective.ObjectiveStates.Complete && baseObjective.Phase == quest.CurrentPhase && baseObjective is ObjectiveCraft)
                            {
                                ObjectiveCraft objectiveCraft = (ObjectiveCraft)baseObjective;
                                list.Add(objectiveCraft.expectedItemClass.Id);
                            }
                        }
                    }
                }
                if (list.Count == 0)
                {
                    return;
                }
                foreach (RecipeQueueItem recipeQueueItem in __instance.workstationData.GetRecipeQueueItems())
                {
                    if (recipeQueueItem.Recipe != null && list.Contains(recipeQueueItem.Recipe.itemValueType))
                    {
                        GameManager.ShowTooltipWithAlert(__instance.xui.playerUI.entityPlayer, Localization.Get("questItemCrafting", Localization.QuestPrefix), "ui_denied");
                        return;
                    }
                }
            }
        }
    }
}
