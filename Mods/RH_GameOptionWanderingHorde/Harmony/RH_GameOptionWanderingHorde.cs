﻿using DMT;
using Harmony;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class RH_GameOptionWanderingHorde
{
    public class RH_GameOptionWanderingHorde_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(AIDirectorWanderingHordeComponent))]
    [HarmonyPatch("SpawnWanderingHorde")]
    [HarmonyPatch(new Type[] { typeof(bool) })]
    public class PatchAIDirectorWanderingHordeComponentSpawnWanderingHorde
    {
        static bool Prefix(AIDirectorWanderingHordeComponent __instance, ref bool feral)
        {
            var maxCount = 1;
          
            switch (GameStats.GetInt(EnumGameStats.WanderingHordeMaxCount))
            {
                // case 0 is default of 1 which is covered by default value of variable maxCount
                case 1:
                    maxCount = 2;
                    break;
                case 2:
                    maxCount = 3;
                    break;
                case 3:
                    maxCount = 4;
                    break;
                case 4:
                    maxCount = 5;
                    break;
                case 5:
                    return true; // Infinte spawns
            }

            if (__instance.spawners.Count >= maxCount)
            {
                __instance.ChooseNextWanderingHordeTime();
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(AIDirectorWanderingHordeComponent))]
    [HarmonyPatch("ChooseNextWanderingHordeTime")]
    public class PatchAIDirectorWanderingHordeComponentChooseNextWanderingHordeTime
    {
        static bool Prefix(AIDirectorWanderingHordeComponent __instance)
        {
            ulong frequency = 0u;

            switch (GameStats.GetInt(EnumGameStats.WanderingHordeFrequency))
            {
                case 0:
                    frequency = (ulong)__instance.Random.RandomRange(12000, 24000);
                    break;
                case 1:
                    frequency = (ulong)__instance.Random.RandomRange(10000, 22000);
                    break;
                case 2:
                    frequency = (ulong)__instance.Random.RandomRange(8000, 20000);
                    break;
                case 3:
                    frequency = (ulong)__instance.Random.RandomRange(6000, 18000);
                    break;
                case 4:
                    frequency = (ulong)__instance.Random.RandomRange(4000, 16000);
                    break;
                case 5:
                    frequency = (ulong)__instance.Random.RandomRange(2000, 14000);
                    break;
            }

            __instance.m_nextWanderingHordeTime = __instance.Director.World.worldTime + frequency; // CHANGED 12000, 24000

            //Log.Out("ChooseNextWanderingHordeTime m_nextWanderingHordeTime : " + __instance.m_nextWanderingHordeTime);

            return false;
        }
    }

    [HarmonyPatch(typeof(AIDirectorHordeComponent))]
    [HarmonyPatch("FindWanderingHordeTargets")]
    public class PatchAIDirectorHordeComponentFindWanderingHordeTargets
    {
        static bool Prefix(AIDirectorHordeComponent __instance, ref uint __result, out Vector3 startPos, out Vector3 pitStop, out Vector3 endPos, List<AIDirectorPlayerState> outTargets)
        {
            startPos = Vector3.zero;
            pitStop = Vector3.zero;
            endPos = Vector3.zero;
            List<AIDirectorPlayerState> list = __instance.Director.GetComponent<AIDirectorPlayerManagementComponent>().trackedPlayers.list;
            int num = __instance.Random.RandomRange(0, list.Count);
            AIDirectorPlayerState aidirectorPlayerState = list[num];
            int num2 = 1;
            while (num2 < list.Count && aidirectorPlayerState.Dead)
            {
                num = (num + num2) % list.Count;
                aidirectorPlayerState = list[num];
                num2++;
            }
            if (aidirectorPlayerState.Dead)
            {
                __result = 1u;
                return false;
            }
            outTargets.Add(aidirectorPlayerState);
            int num3 = 1;
            pitStop = aidirectorPlayerState.Player.position;
            pitStop.y = 0f;
            for (int i = 0; i < list.Count; i++)
            {
                AIDirectorPlayerState aidirectorPlayerState2 = list[i];
                if (aidirectorPlayerState2 != aidirectorPlayerState)
                {
                    Vector3 vector = aidirectorPlayerState2.Player.position - aidirectorPlayerState.Player.position;
                    vector.y = 0f;
                    if (vector.sqrMagnitude <= 900f)
                    {
                        pitStop.x += aidirectorPlayerState2.Player.position.x;
                        pitStop.z += aidirectorPlayerState2.Player.position.z;
                        num3++;
                        outTargets.Add(aidirectorPlayerState2);
                    }
                }
            }

            var random = __instance.Random.RandomFloat;
            if (num3 == 1 && random < 0.3f)
            {
                __result = GetDelay(); // CHANGED __result = 12u;
                return false;
            }
            pitStop /= (float)num3;
            pitStop.y = 255f;
            if (!__instance.AdjustFindPos(ref pitStop))
            {
                Log.Out("AIDirector: FindWanderingHordeTargets pitstop y < 0");
                __result = 1u;
                return false;
            }
            int num4 = 0;
            int num5 = 0;
            int num6 = Utils.Fastfloor(pitStop.y - 1f);
            Vector2 randomOnUnitCircle = __instance.Random.RandomOnUnitCircle;
            float num7 = 92f;
            int num8 = 5;
            while (num7 > 0f)
            {
                Vector2 vector2 = randomOnUnitCircle * num7;
                num4 = Utils.Fastfloor(pitStop.x + vector2.x);
                num5 = Utils.Fastfloor(pitStop.z + vector2.y);
                Chunk chunk = (Chunk)__instance.Director.World.GetChunkFromWorldPos(num4, num6, num5);
                if (chunk != null && __instance.Director.World.GetChunkFromWorldPos(num4 - 16, num6, num5 - 16) != null && __instance.Director.World.GetChunkFromWorldPos(num4 + 16, num6, num5 + 16) != null)
                {
                    if (num8 > 0)
                    {
                        bool flag = false;
                        for (int j = 0; j < list.Count; j++)
                        {
                            AIDirectorPlayerState aidirectorPlayerState3 = list[j];
                            if (!aidirectorPlayerState3.Dead)
                            {
                                Vector3 vector3 = aidirectorPlayerState3.Player.position - new Vector3((float)num4, (float)num6, (float)num5);
                                vector3.y = 0f;
                                if (vector3.sqrMagnitude < 900f)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (flag)
                        {
                            num7 = 92f;
                            randomOnUnitCircle = __instance.Random.RandomOnUnitCircle;
                            num8--;
                            continue;
                        }
                    }
                    num6 = (int)pitStop.y + 32;
                    while (__instance.Director.World.GetBlock(num4, num6, num5).type == 0)
                    {
                        if (--num6 < 0)
                        {
                            Log.Out("AIDirector: FindWanderingHordeTargets start y < 0");
                            __result = 1u;
                            return false;
                        }
                    }
                    if (chunk.CanMobsSpawnAtPos(World.toBlockXZ(num4), num6 + 1, World.toBlockXZ(num5), false))
                    {
                        break;
                    }
                    if (num8 < 1)
                    {
                        __result = 2u;
                        return false;
                    }
                    num7 = 92f;
                    randomOnUnitCircle = __instance.Random.RandomOnUnitCircle;
                    num8--;
                }
                else
                {
                    num7 -= 16f;
                }
            }
            if (num7 < 50f)
            {
                Log.Out("AIDirector: FindWanderingHordeTargets start too close {0}", new object[]
                {
                num7
                });
                __result = 1u;
                return false;
            }
            startPos = new Vector3((float)num4, (float)(num6 + 1), (float)num5);
            Vector2 vector4 = Vector2.Perpendicular(randomOnUnitCircle);
            if (__instance.Random.RandomFloat < 0.5f)
            {
                vector4 *= -1f;
            }
            vector4 *= 40f + __instance.Random.RandomFloat * 20f;
            pitStop.x += vector4.x;
            pitStop.z += vector4.y;
            pitStop.y = 255f;
            if (!__instance.AdjustFindPos(ref pitStop))
            {
                Log.Out("AIDirector: FindWanderingHordeTargets pitstop side y < 0");
                __result = 1u;
                return false;
            }
            endPos.x = pitStop.x - startPos.x + pitStop.x;
            endPos.z = pitStop.z - startPos.z + pitStop.z;
            endPos.y = pitStop.y + 32f;
            if (!__instance.AdjustFindPos(ref endPos))
            {
                Log.Out("AIDirector: FindWanderingHordeTargets end y < 0");
                __result = 1u;
                return false;
            }
            if (AIDirectorConstants.DebugOutput)
            {
                Log.Out("AIDirector: FindWanderingHordeTargets at player '{0}', dist {1}", new object[]
                {
                aidirectorPlayerState.Player,
                vector4.magnitude
                });
            }
            __result = 0u;
            return false;
        }
    }

    [HarmonyPatch(typeof(AIDirectorGameStagePartySpawner))]
    [HarmonyPatch("NextStage")]
    [HarmonyPatch(new Type[] { typeof(int) })]
    public class Patch_AIDirectorGameStagePartySpawner_NextStage
    {
        static bool Prefix(AIDirectorGameStagePartySpawner __instance, ref int _index)
        {
            __instance.spawn = __instance.stage.GetSpawn(_index);
            if (__instance.spawn != null)
            {
                __instance.interval = (double)__instance.spawn.interval;
                __instance.nextStageTime = ((__instance.spawn.duration > 0UL) ? (__instance.world.worldTime + __instance.spawn.duration * 1000UL) : 0UL);
                __instance.numToSpawn = EntitySpawner.ModifySpawnCountByGameDifficulty(__instance.spawn.spawnCount) * (GameStats.GetInt(EnumGameStats.WanderingHordeMultiplier) + 1);
                __instance.spawnCount = 0;
                return false;
            }
            Log.Out("AIDirectorGameStagePartySpawner: NextStage done ({0})", new object[]
            {
            _index
            });

            return false;
        }
    }

    private static uint GetDelay()
    {
        switch (GameStats.GetInt(EnumGameStats.GameDifficulty))
        {
            case 0:
                return 12u;
            case 1:
                return 10u;
            case 2:
                return 8u;
            case 3:
                return 6u;
            case 4:
                return 4u;
            case 5:
                return 2u;
        }

        return 12u;
    }
}

