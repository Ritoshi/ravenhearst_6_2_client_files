﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;

public class RH_InfiniteRecipeIngredients
{

    /* NOTE
        * File : windows.xml
        * Window : craftingInfoPanel
        * 
        * Add following new UI : 
        * 	
        * 	<!-- NEW PAGINATION -->
		<panel pos="480,0" width="104" height="43" disableautobackground="true">
			<button depth="4" name="pageDown" style="icon30px, press" pos="20,-22" sprite="ui_game_symbol_arrow_left" pivot="center" sound="[paging_click]" />
			<rect depth="4" pos="37,-7" >
				<sprite name="background" style="icon30px" color="[black]" type="sliced" />
				<label depth="5" name="pageNumber" pos="0, -3" width="30" height="28" text="1" font_size="26" justify="center"/>
			</rect>
			<button depth="4" name="pageUp" style="icon30px, press" pos="84,-22" sprite="ui_game_symbol_arrow_right" pivot="center" sound="[paging_click]" />
		</panel>
		<!-- NEW PAGINATION -->
         
        Add the above section ABOVE the following existing line : <rect depth="1" pos="153,-124" name="ingredients" width="447" height="264">
        * */

    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_IngredientList))]
    [HarmonyPatch("Init")]  
    class PatchXUiC_IngredientListInit
    {
        static void Postfix(XUiC_IngredientList __instance)
        {
            __instance.Parent.Parent.GetChildById("pageUp").OnPress += new XUiEvent_OnPressEventHandler(__instance.HandlePageUpPress);
            __instance.Parent.Parent.GetChildById("pageDown").OnPress += new XUiEvent_OnPressEventHandler(__instance.HandlePageDownPress);
            __instance.btnPageDown = (__instance.Parent.Parent.GetChildById("pageDown").ViewComponent as XUiV_Button);
            __instance.btnPageUp = (__instance.Parent.Parent.GetChildById("pageUp").ViewComponent as XUiV_Button);
            __instance.lblPageNumber = (__instance.Parent.Parent.GetChildById("pagenumber").ViewComponent as XUiV_Label);
        }
    }

    [HarmonyPatch(typeof(XUiC_IngredientList))]
    [HarmonyPatch("Recipe", MethodType.Setter)]
    class PatchXUiC_IngredientListRecipeSetter
    {
        static void Postfix(XUiC_IngredientList __instance)
        {
            if (__instance.recipe != null)
            {
                __instance.btnPageDown.IsVisible = false;

                if (__instance.recipe.ingredients.Count <= __instance.ingredientEntries.Count)
                    __instance.btnPageUp.IsVisible = false;
                else
                    __instance.btnPageUp.IsVisible = true;

                __instance.indicatorCount = 0;
            }
        }
    }

    [HarmonyPatch(typeof(XUiC_IngredientList))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    class PatchXUiC_IngredientListUpdate
    {
        static bool Prefix(XUiC_IngredientList __instance, ref float _dt)
        {
            if (__instance.indicatorCount < 1 && !__instance.btnPageUp.isOver)
            {
                var indicator = GameTimer.Instance.ticks / 20;
                if (indicator % 2 == 0)
                {
                    __instance.btnPageUp.CurrentColor = __instance.btnPageUp.DefaultSpriteColor;
                }
                else
                {
                    __instance.btnPageUp.CurrentColor = new Color(0.1921f, 0.3568f, 0.7960f);
                }
            }

            if(__instance.btnPageUp.isOver && __instance.indicatorCount < 1)
                __instance.indicatorCount = 1;

            if (__instance.isDirty)
            {
                if (__instance.recipe != null)
                {
                    int count = __instance.ingredientEntries.Count;
                    int count2 = __instance.recipe.ingredients.Count;
                    for (int i = 0; i < count; i++)
                    {
                        if (__instance.ingredientEntries[i] is XUiC_IngredientEntry)
                        {
                            int num = __instance.page * count + i;
                            ((XUiC_IngredientEntry)__instance.ingredientEntries[i]).Ingredient = ((num >= count2) ? null : __instance.recipe.ingredients[num]);
                        }
                    }
                    __instance.lblPageNumber.Text = (__instance.page + 1).ToString();

                    // Hide/show PageDown button 
                    if(__instance.page == 0)
                        __instance.btnPageDown.IsVisible = false;
                    else
                        __instance.btnPageDown.IsVisible = true;

                    // Hide/show PageUp button 
                    if ((__instance.page + 1) * 5 >= __instance.recipe.ingredients.Count)
                        __instance.btnPageUp.IsVisible = false;
                    else
                        __instance.btnPageUp.IsVisible = true;
                }
                else
                {
                    int count3 = __instance.ingredientEntries.Count;
                    for (int j = 0; j < count3; j++)
                    {
                        if (__instance.ingredientEntries[j] is XUiC_IngredientEntry)
                        {
                            ((XUiC_IngredientEntry)__instance.ingredientEntries[j]).Ingredient = null;
                        }
                    }
                }
                __instance.isDirty = false;
            }

            return true;
        }
    }
        
    [HarmonyPatch(typeof(XUiC_IngredientList))]
    [HarmonyPatch("HandlePageDownPress")]
    [HarmonyPatch(new Type[] { typeof(XUiController), typeof(EventArgs) })]
    class PatchXUiC_IngredientListHandlePageDownPress
    {
        static bool Prefix(XUiC_IngredientList __instance, ref XUiController _sender, ref EventArgs _e)
        {
            if (__instance.page > 0)
            {
                int num = __instance.page;
                __instance.page = num - 1;
                __instance.isDirty = true;
                __instance.pageChanged = true;
            }

            return false;
        }
    }

    [HarmonyPatch(typeof(XUiC_IngredientList))]
    [HarmonyPatch("HandlePageUpPress")]
    [HarmonyPatch(new Type[] { typeof(XUiController), typeof(EventArgs) })]
    class PatchXUiC_IngredientListHandlePageUpPress
    {
        static bool Prefix(XUiC_IngredientList __instance, ref XUiController _sender, ref EventArgs _e)
        {
            //__instance.HandlePageUp();

            if ((__instance.page + 1) * 5 < __instance.recipe.ingredients.Count)
            {
                int num = __instance.page;
                __instance.page = num + 1;
                __instance.isDirty = true;
                __instance.pageChanged = true;
            }

            return false;
        }
    }

    [HarmonyPatch(typeof(XUiC_IngredientList))]
    [HarmonyPatch("OnOpen")]
    class PatchXUiC_IngredientListOnOpen
    {
        static bool Prefix(XUiC_IngredientList __instance)
        {
            __instance.page = 0;
            __instance.indicatorCount = 0;

            return true;
        }
    }
}
